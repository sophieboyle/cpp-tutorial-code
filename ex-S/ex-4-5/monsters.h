#ifndef MONSTERS_H
#define MONSTERS_H

// enum type holding: orcs, goblins, trolls, ogres, and skeletons.

enum Monster
{
    MONSTER_ORC,
    MONSTER_GOBLIN,
    MONSTER_TROLL,
    MONSTER_OGRE,
    MONSTER_SKELETON,
};

#endif