#include <iostream>

/*
Gets two integers from user
If the first int is larger than the second int
Swap the integers
*/
int main()
{
    int x{};
    int y{};

    std::cout << "Enter an integer: ";
    std::cin >> x;

    std::cout << "Enter a larger integer: ";
    std::cin >> y;

    if (x>y)
    {
        std::cout << "Swapping the values.\n";

        int a{x};
        int b{y};

        x = b;
        y = a;
    }

    std::cout << "The smaller value is " << x << '\n';
    std::cout << "The larger value is " << y;

    return 0;
}