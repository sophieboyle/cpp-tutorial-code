/*
@brief Returns a true for the first three times it is called, and false thereafter
@returns bool
*/
bool passOrFail()
{
    static int count{0};
    ++count;
    
    if (count<4)
        return true;
    else
        return false;
}