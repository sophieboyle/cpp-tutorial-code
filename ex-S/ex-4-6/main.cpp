// Pre C++11
typedef int error_t;
error_t printData();

// Post C++11
using error_t = int;
error_t printData();