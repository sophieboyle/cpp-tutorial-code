#include "fraction.h"
#include "helper.h"
#include <iostream>
#include <iomanip>

int main()
{
    Fraction myFraction{getFractionFields()};
    Fraction yourFraction{getFractionFields()};
    std::cout << std::setprecision(3);
    std::cout << "The multiple of your fractions are: " << multiply(myFraction, yourFraction);
}