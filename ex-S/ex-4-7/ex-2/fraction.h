#ifndef FRACTION_H
#define FRACTION_H

/*
@brief Fraction struct to hold the properties of a fraction
@field numerator: the fraction's integer numerator
@field denominator: the fraction's integer denominator
*/
struct Fraction
{
    int numerator;
    int denominator;
};

#endif