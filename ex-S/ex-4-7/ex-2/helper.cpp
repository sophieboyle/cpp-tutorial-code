#include "helper.h"
#include <iostream>

/*
@brief Takes two fraction structs and returns the double multiple of both
@param fraction1: the first fraction operand
@param fraction2: the second fraction operand
@returns double: the multiple of both fractions
*/
double multiply(Fraction fraction1, Fraction fraction2)
{
    return static_cast<double>(fraction1.numerator*fraction2.numerator) / (fraction1.denominator*fraction2.denominator);
}

/*
@brief Gets fraction fields from user input
@returns Fraction structure consisting of user allocated fields
*/
Fraction getFractionFields()
{   
    int numerator{};
    int denominator{};

    std::cout << "Enter a numerator: ";
    std::cin >> numerator;
    std::cout << "Enter a denominator: ";
    std::cin >> denominator;

    return {numerator, denominator};
}