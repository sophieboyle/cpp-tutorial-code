#ifndef HELPER_H
#define HELPER_H

#include "fraction.h"

double multiply(Fraction fraction1, Fraction fraction2);
Fraction getFractionFields();

#endif