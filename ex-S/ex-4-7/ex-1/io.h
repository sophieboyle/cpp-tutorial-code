#ifndef IO_H
#define IO_H

#include "advertising.h"

Advertising getAdvertisingFields();
double calcEarning(Advertising advertising);
void outputAdvertising(Advertising advertising);

#endif