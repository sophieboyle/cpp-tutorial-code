#include "advertising.h"
#include <iostream>

/*
@brief Gets the fields for an advertising struct from user input
@returns The insansiated advertising struct
*/
Advertising getAdvertisingFields()
{
    int adsShown{};
    double percentageClicked{};
    double earningPerAd{};

    std::cout << "Enter ads shown: ";
    std::cin >> adsShown;
    std::cout << "Enter percentage clicked: ";
    std::cin >> percentageClicked;
    std::cout << "Enter earning per ad: ";
    std::cin >> earningPerAd;

    return {adsShown, percentageClicked, earningPerAd};
}

/*
@brief Takes an advertising struct, and returns your earning for a day
@param Advertising, your advertising struct
@returns Earnings for the day
*/
double calcEarning(Advertising advertising)
{
    return advertising.adsShown * advertising.earningPerAd * (advertising.percentageClicked/100);
}

/*
@brief Given an advertising struct, outputs the fields to console in a readable fashion
@param advertising: the advertising struct for which the fields are to be output
*/
void outputAdvertising(Advertising advertising)
{
    std::cout << "The overall amount of ads shown to users: " << advertising.adsShown << '\n';
    std::cout << "The percentage of ads clicked by users: " << advertising.percentageClicked << "%\n";
    std::cout << "The money earned per ad clicked: $" << advertising.earningPerAd << '\n';
}