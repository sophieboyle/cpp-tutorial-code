#include "advertising.h"
#include "io.h"
#include <iostream>

int main()
{
    Advertising myAdv{getAdvertisingFields()};
    outputAdvertising(myAdv);
    std::cout << "Your daily earnings are: $" << calcEarning(myAdv);
}