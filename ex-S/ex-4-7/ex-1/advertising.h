#ifndef ADVERTISING_H
#define ADVERTISING_H

/*
@brief Structure representing advertisement stats
@field adsShown: ads shown to users overall
@field percentageClicked: percentage of overall ads clicked by users
@field earningPerAd:  money earned per ad clicked
*/
struct Advertising
{
    int adsShown;
    double percentageClicked;
    double earningPerAd;
};

#endif