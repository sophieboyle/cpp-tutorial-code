#include "monsters.h"
#include "helper.h"
#include <string>

int main()
{
    MonsterInfo torg{MONSTER_OGRE, "Torg", 145};
    MonsterInfo blrup{MONSTER_SLIME, "Blurp", 23};

    printMonster(torg);
    printMonster(blrup);

    return 0;
}