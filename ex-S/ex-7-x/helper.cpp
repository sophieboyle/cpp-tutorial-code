#include "monsters.h"
#include "helper.h"
#include <iostream>

/*
@brief Outputs the given monster's details to console
@param MonsterInfo: The given monster to output
*/
void printMonster(MonsterInfo monster)
{
    std::cout << "This " << getMonsterTypeString(monster.type) << " is named ";
    std::cout << monster.name << " and has " << monster.health << " health\n";
}

/*
@brief Returns a string representation of the monster's type
@param Monster: The type of the monster to be represented as string
@param std::string: The string representation of the type
*/
std::string getMonsterTypeString(Monster type)
{
    if (type==MONSTER_OGRE)
        return "Ogre";
    else if (type==MONSTER_DRAGON)
        return "Dragon";
    else if (type==MONSTER_ORC)
        return "Orc";
    else if (type==MONSTER_GIANT_SPIDER)
        return "Giant Spider";
    else if (type==MONSTER_SLIME)
        return "Slime";
    else
        return "???";
}