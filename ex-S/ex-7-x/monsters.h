#ifndef MONSTERS_H
#define MONSTERS_H

#include <string>

/*
@brief Monster enum representing different types of monsters
*/
enum Monster
{
    MONSTER_OGRE,
    MONSTER_DRAGON,
    MONSTER_ORC,
    MONSTER_GIANT_SPIDER,
    MONSTER_SLIME,
};

/*
@brief MonsterInfo struct holding the details of an individual monster
@field Monster type: The type of monster it is
@field std::string name: The name of the monster
@field int health: The monster's health
*/
struct MonsterInfo
{
    Monster type;
    std::string name;
    int health;
};

#endif