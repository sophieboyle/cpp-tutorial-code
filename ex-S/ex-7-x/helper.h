#ifndef HELPER_H
#define HELPER_H

#include "monsters.h"

void printMonster(MonsterInfo monster);
std::string getMonsterTypeString(Monster type);

#endif