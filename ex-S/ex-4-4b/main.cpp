#include "io.h"
#include "calc.h"
#include <iostream>
#include <string>

int main()
{
    using namespace std;
    
    string name{io::getName()};
    int age{io::getAge()};

    cout << "You've lived " << calcYearsPerLetter(name.length(), age);
    cout << " for each letter in your name";

    return 0;
}