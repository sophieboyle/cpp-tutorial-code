#ifndef IO_H
#define IO_H

#include <string>

namespace io
{
    std::string getName();
    int getAge();
}

#endif