#include "io.h"
#include <iostream>
#include <string>

using namespace std;

namespace io
{
    string getName()
    {
        string name{};
        cout << "Enter your full name: ";
        getline(cin, name);
        // No cin.ignore() is required here, as getline does not leave trailing \n
        return name;
    }
    
    int getAge()
    {
        int age{};
        cout << "Enter your age: ";
        cin >> age;
        cin.ignore(); // cin.ignore() required as std::cin leaves trailing \n
        return age;
    }
}