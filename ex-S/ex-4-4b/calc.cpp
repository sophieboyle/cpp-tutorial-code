float calcYearsPerLetter(int chars, int age)
{
    return static_cast<float>(age) / chars;
}