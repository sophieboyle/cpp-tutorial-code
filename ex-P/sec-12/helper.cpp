#include "helper.h"


std::string getUsername()
{
    std::string username{};
    std::cout << "Enter username: ";
    std::getline(std::cin, username);
    return username;
}


bool nameFound(std::string username)
{
    std::string names[]{
        "Alex", "Betty, Caroline, Dave",
        "Emily", "Fred", "Greg", "Holly"};

    for (std::string name : names)
    {
        if (username == name)
            return true;
    }

    return false;
}