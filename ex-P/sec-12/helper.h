#ifndef HELPER_H
#define HELPER_H

#include <string>
#include <iostream>
#include <sstream>

/*
@brief Gets a username string from user input.
@return std::string: Username.
*/
std::string getUsername();

/*
@brief Checks if a given username is in the predefined set of names.
Set of names is defined inside the function.
@return bool: true if found, false otherwise.
*/
bool nameFound(std::string username);

#endif