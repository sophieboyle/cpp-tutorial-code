#include "helper.h"
#include <iostream>
#include <sstream>

int main()
{
    const std::string username{getUsername()};
    std::cout << username << " was " << (nameFound(username) ? "found" : "not found");
    return 0;
}