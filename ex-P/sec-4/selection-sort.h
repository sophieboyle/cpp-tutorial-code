#ifndef SELECTION_SORT_H
#define SELECTION_SORT_H

/*
@brief Given the array and the array's length, sort the array in ascending order.
Since the array is altered in-place, there is no need to return it.
@param int array[]: The array to be sorted.
@param int length: The length of the array.
*/
void selectionSortAsc(int array[], int length);


/*
@brief Given the array and the array's length, sort the array in descending order.
Since the array is altered in-place, there is no need to return it.
@param int array[]: The array to be sorted.
@param int length: The length of the array.
*/
void selectionSortDesc(int array[], int length);

#endif