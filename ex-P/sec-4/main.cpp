#include "selection-sort.h"
#include "bubble-sort.h"
#include <iostream>


/*
@brief Outputs elements of an array.
@param int array[]: The array to be output.
@param int length: The length of the array.
*/
void outArray(int array[], int length)
{
    for (int i{0}; i < length; ++i)
        std::cout << array[i] << ' ';
    std::cout << '\n';
}


int main()
{
    int array[]{30, 50, 20, 10, 40};
    int arrayLen{sizeof(array)/sizeof(array[0])};

    selectionSortAsc(array, arrayLen);
    std::cout << "Selection Sort in Ascending Order: ";
    outArray(array, arrayLen);

    selectionSortDesc(array, arrayLen);
    std::cout << "Selection Sort in Descending Order: ";
    outArray(array, arrayLen);

    int array2[]{6, 3, 2, 9, 7, 1, 5, 4, 8};
    int arrayLen2{sizeof(array2)/sizeof(array2[0])};

    bubbleSortAsc(array2, arrayLen2);
    std::cout << "Bubble Sort in Ascending Order: ";
    outArray(array2, arrayLen2);

    return 0;
}