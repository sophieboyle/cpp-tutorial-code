#include <utility>

void selectionSortAsc(int array[], int length)
{
    for (int index{0}; index<length-1; ++index)
    {
        int minIndex{index};
        for (int innerIndex{index+1}; innerIndex<length; ++innerIndex)
        {
            if (array[minIndex] > array[innerIndex])
                minIndex = innerIndex;
        }
        std::swap(array[index], array[minIndex]);
    }
}


void selectionSortDesc(int array[], int length)
{
    for (int index{0}; index<length-1; ++index)
    {
        int maxIndex{index};
        for (int innerIndex{index+1}; innerIndex<length; ++innerIndex)
        {
            if (array[maxIndex]<array[innerIndex])
                maxIndex = innerIndex;
        }
        std::swap(array[index], array[maxIndex]);
    }
}