#include <utility>
#include <iostream>

void bubbleSortAsc(int array[], int length)
{   
    while (true)
    {
        static int iteration{0};
        int swapped{0};

        for (int index{0}; index<length-1; ++index)
        {   
            if (array[index] > array[index+1])
            {
                std::swap(array[index], array[index+1]);
                ++swapped;
            }
        }

        if (swapped == 0)
        {
            std::cout << "Early termination on iteration " << iteration << '\n';
            return;
        }

        ++iteration;
    }
}