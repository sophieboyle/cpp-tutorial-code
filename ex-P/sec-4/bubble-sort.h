#ifndef BUBBLE_SORT_H
#define BUBBLE_SORT_H

/*
@brief Sorts the given array in-place in ascending order using bubble sort.
@param int array[]: Array to be sorted.
@param int length: Length of array to be sorted.
*/
void bubbleSortAsc(int array[], int length);

#endif