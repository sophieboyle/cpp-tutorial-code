#ifndef ANIMALS_H
#define ANIMALS_H

namespace Animals
{
    enum Types
    {
        CHICKEN,
        DOG,
        CAT,
        ELEPHANT,
        DUCK,
        SNAKE,
        MAX_SIZE,
    };
}

/*
@brief Returns the number of legs a given animal has.
@param Animals::Types animaltype: The type of animal to be given.
@return int: Number of legs.
*/
int getLegs(Animals::Types animaltype);

#endif