#include "animals.h"
#include <iostream>

int main()
{
    int animalLegs[Animals::Types::MAX_SIZE]{
        getLegs(Animals::Types::CAT),
        getLegs(Animals::Types::DOG),
        getLegs(Animals::Types::CHICKEN),
        getLegs(Animals::Types::DUCK),
        getLegs(Animals::Types::ELEPHANT),
        getLegs(Animals::Types::SNAKE)
    };

    for (int i{0}; i < Animals::Types::MAX_SIZE; ++i)
    {
        std::cout << animalLegs[i] << '\n';
    }

    return 0;
}