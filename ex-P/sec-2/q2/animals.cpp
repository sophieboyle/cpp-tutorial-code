#include "animals.h"

using namespace Animals;

int getLegs(Types animaltype)
{
    switch(animaltype)
    {
        case Types::CAT:
        case Types::DOG:
        case Types::ELEPHANT:
            return 4;
        case Types::DUCK:
        case Types::CHICKEN:
            return 2;
        case Types::SNAKE:
            return 0;
        default:
            return -1; // error checking
    }
}