#include "io.h"
#include <iostream>

int main()
{
    int array[]{4, 6, 7, 3, 8, 2, 1, 9, 5};
    int arrayLen{sizeof(array)/sizeof(array[0])};

    int userElement{getUserInput(1, 9)};
    int userIndex{-1}; // Indicative of userElement not being in the array

    std::cout << "The array is: ";

    for (int i{0}; i < arrayLen; ++i)
    {   
        std::cout << array[i] << ' ';
        
        if (array[i]==userElement)
        {
            userIndex = i;
        }
    }

    std::cout << "\nThe index of your chosen element is: " << userIndex;

    return 0;
}