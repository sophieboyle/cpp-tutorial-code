#ifndef IO_H
#define IO_H

/*
@brief Prompts user to input an integer between min and max.
@param int min: The minimum integer an input can be.
@param int max: The maximum integer an input can be.
*/
int getUserInput(int min, int max);

#endif