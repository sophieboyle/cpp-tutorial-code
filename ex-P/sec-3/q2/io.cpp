#include "io.h"
#include <iostream>

int getUserInput(int min, int max)
{
    while (true)
    {   
        int userIn{};
        std::cout << "Pick a number between 1 and 9: ";
        std::cin >> userIn;

        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(32767, '\n');
            continue;
        }

        std::cin.ignore(32767, '\n');

        if ((userIn < min) || (userIn > max))
        {
            std::cout << "That is an invalid number\n";
            continue;
        }
        else
        {
            return userIn;
        }
    }
}