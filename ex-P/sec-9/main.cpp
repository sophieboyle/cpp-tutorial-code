#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>

int main()
{
    int namesAmount{};
    std::cout << "Enter the number of names: ";
    std::cin >> namesAmount;
    std::cin.ignore();

    std::string *names{new std::string[namesAmount]{}};

    for (int nameNum{0}; nameNum < namesAmount; ++nameNum)
    {
        std::cout << "Enter name no. " << nameNum+1<< ": ";
        std::getline(std::cin, names[nameNum]);
    }
    
    std::sort(names, names+namesAmount);

    for (int nameNum{0}; nameNum < namesAmount; ++nameNum)
    {
        std::cout << names[nameNum] << '\n';
    }

    return 0;
}