#include "cards.h"
#include <iostream>
#include <array>
#include <algorithm>
#include <ctime>
#include <random>
#include <numeric>
#include <vector>


void printCard(const Card &card)
{   
    switch (card.rank)
    {
        case (Rank::TWO):
        case (Rank::THREE):
        case (Rank::FOUR):
        case (Rank::FIVE):
        case (Rank::SIX):
        case (Rank::SEVEN):
        case (Rank::EIGHT):
        case (Rank::NINE):
        case (Rank::TEN):
            std::cout << static_cast<int>(card.rank);
            break;
        case (Rank::JACK):
            std::cout << 'J';
            break;
        case (Rank::QUEEN):
            std::cout << 'Q';
            break;
        case (Rank::KING):
            std::cout << 'K';
            break;
        case (Rank::ACE):
            std::cout << 'A';
            break;
        default:
            std::cout << '?';
    }

    switch (card.suit)
    {
        case (Suit::SPADES):
            std::cout << "S";
            break;
        case (Suit::DIAMONDS):
            std::cout << "D";
            break;
        case (Suit::HEARTS):
            std::cout << "H";
            break;
        case (Suit::CLUBS):
            std::cout << "C";
            break;
        default:
            std::cout << "?";
    }

    std::cout << '\n';

}


std::array<Card, 52> createDeck()
{
    std::array<Card, 52> deck;
    int deckIndex{0};

    for (int rank {static_cast<int>(Rank::TWO)}; rank != static_cast<int>(Rank::MAX_RANK); ++rank)
    {
        for (int suit {static_cast<int>(Suit::CLUBS)}; suit != static_cast<int>(Suit::MAX_SUIT); ++suit)
        {
            deck[deckIndex] = Card {static_cast<Rank>(rank), static_cast<Suit>(suit)};
            ++deckIndex;
        }
    }

    return deck;
}


void printDeck(const std::array<Card, 52> &deck)
{
    for (auto &card : deck)
        printCard(card);
}


void shuffleDeck(std::array<Card,52> &deck)
{
    std::mt19937 mt{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };
    std::shuffle(deck.begin(), deck.end(), mt);
}


int getCardValue(const Card &card)
{
    switch (card.rank)
    {
        case (Rank::TWO):
        case (Rank::THREE):
        case (Rank::FOUR):
        case (Rank::FIVE):
        case (Rank::SIX):
        case (Rank::SEVEN):
        case (Rank::EIGHT):
        case (Rank::NINE):
        case (Rank::TEN):
            return static_cast<int>(card.rank);
        case (Rank::JACK):
        case (Rank::QUEEN):
        case (Rank::KING):
            return 10;
        case (Rank::ACE):
            return 11;
        default:
            return 0;
    }
}


bool playBlackJack(const std::array<Card, 52> &deck)
{
    int index{0};

    //Player turn
    std::vector<Card> playerHand{deck[index], deck[++index]};
    int playerScore{playerTurn(playerHand, index, deck)};

    if (playerScore > 21)
    {
        std::cout << "You have gone bust.\n";
        return false;
    }

    // Dealer turn
    std::vector<Card> dealerHand{deck[++index]};
    int dealerScore{dealerTurn(dealerHand, index, deck)};

    if (dealerScore > 21)
    {
        std::cout << "The dealer has gone bust.\n";
        return true;
    }

    return (playerScore > dealerScore);
}


int calcScore(const std::vector<Card> &playerHand)
{
    int playerScore{0};
    for (auto &card: playerHand)
    {   
        std::cout << "DEBUG: ";
        printCard(card);
        if ((card.rank == Rank::ACE) && ((playerScore + getCardValue(card)) > 21))
            playerScore += 1;
        else
            playerScore += getCardValue(card);
    }
    return playerScore;
}


int playerTurn(std::vector<Card> &playerHand, int &index, const std::array<Card, 52> &deck)
{
    int playerScore{calcScore(playerHand)};

    while (playerScore <= 21)
    {
        std::cout << "Your score is currently: " << playerScore << '\n';

        char hitStand{};
        std::cout << "Would you like to hit or stand? (h/s): ";
        std::cin >> hitStand;
        
        if (hitStand=='h')
        {
            playerHand.insert(playerHand.begin(), deck[++index]);
            playerScore = calcScore(playerHand);
        }
        else
            break;
    }

    return playerScore;
}


int dealerTurn(std::vector<Card> &dealerHand, int &index, const std::array<Card, 52> &deck)
{
    int dealerScore{calcScore(dealerHand)};
   
    while(dealerScore < 17)
    {
        dealerHand.insert(dealerHand.begin(), deck[++index]);
        dealerScore = calcScore(dealerHand);
    }

    return dealerScore;
}