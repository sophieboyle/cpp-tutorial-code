#include "cards.h"
#include <array>
#include <iostream>

int main()
{   
    Card a{Rank::ACE, Suit::SPADES};
    Card b{Rank::EIGHT, Suit::HEARTS};

   std::array<Card, 52> deck;
   deck = createDeck();
   shuffleDeck(deck);
   // printDeck(deck);

   // std::cout << "A's value: " << getCardValue(a) << '\n';
   // std::cout << "B's value: " << getCardValue(b) << '\n';

    std::cout << "Playing BlackJack...\n";
    std::cout << (playBlackJack(deck) ? "You have won" : "You have lost") << '\n';

    return 0;
}