#ifndef CARDS_H
#define CARDS_H

#include <array>
#include <vector>

/*
@brief Represents the rank of a card.
*/
enum class Rank
{
    TWO = 2,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    ACE,
    MAX_RANK
};

/*
@brief Represents the suit of a card.
*/
enum class Suit
{
    CLUBS,
    DIAMONDS,
    HEARTS,
    SPADES,
    MAX_SUIT
};

/*
@brief Represents a card with a rank and suit.
*/
struct Card
{
    Rank rank{};
    Suit suit{};
};

/*
@brief Prints a two-character code representing a card's rank and suit.
@param const Card &card: The card to be printed to console.
*/
void printCard(const Card &card);

/*
@brief Constructs an array of cards representing a deck.
@retun std::array<Card, 52>: Array of cards, set to 52 as the size of a generic card deck.
*/
std::array<Card, 52> createDeck();

/*
@brief Outputs the two-character codes of all of the cards in a given deck.
@param const std::array<Card, 52> &deck: An array of cards assuming standard deck size.
*/
void printDeck(const std::array<Card, 52> &deck);

/*
@brief Shuffles the given deck in-place.
@param std::array<Card, 52> &deck: The deck to be shuffled.
*/
void shuffleDeck(std::array<Card,52> &deck);

/*
@brief Returns an integer value based on the given card's rank.
@param const Card &card: The card to deduce the value of.
@return int: The card's value.
*/
int getCardValue(const Card &card);

/*
@brief Plays a game of black jack with a given shuffled deck.
@param const std::array<Card, 52>&: Shuffled deck of cards, size 52.
@return bool: True if player won, false if player lost.
*/
bool playBlackJack(const std::array<Card, 52> &deck);

/*
@brief Calculates a player's score based on a given hand.
@param std::vector<Card>&: The player's hand.
@return int: Player's score.
*/
int calcScore(const std::vector<Card> &playerHand);

/*
@brief Executes the player turn by prompting the user to hit or stand.
@param std::vector<Card>&: Player's hand.
@param int&: Index (of the deck being used.)
@param const std::array<Card, 52>&: Deck of 52 cards.
@return int: Player's score after this turn.
*/
int playerTurn(std::vector<Card> &playerHand, int &index, const std::array<Card, 52> &deck);

/*
@brief Executes the dealer turn by drawing cards until the score is at least 17.
@param std::vector<Card>&: Dealer's hand.
@param int&: Index (of the deck being used.)
@param const std::array<Card, 52>&: Deck of 52 cards.
@return int: Dealer's score.
*/
int dealerTurn(std::vector<Card> &dealerHand, int &index, const std::array<Card, 52> &deck);

#endif