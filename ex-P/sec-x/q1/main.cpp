#include "inventory.h"
#include <iostream>

int main()
{
    std::array<int, Items::MAX_ITEMS> inventory{2, 5, 10};
    std::cout << "The total number of items is: " << countTotalItems(inventory) << '\n';
    std::cout << "The number of torches is: " << countItem(inventory, Items::TORCHES) << '\n';

    return 0;
}