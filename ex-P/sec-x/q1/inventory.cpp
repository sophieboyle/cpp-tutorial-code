#include "inventory.h"
#include <algorithm>
#include <array>
#include <numeric>


int countTotalItems(const std::array<int, Items::MAX_ITEMS> &items)
{   
    int total{0};
    total = std::accumulate(items.begin(), items.end(), total);
    return total;
}


int countItem(const std::array<int, Items::MAX_ITEMS> &items, Items itemType)
{
    return items[itemType];
}