#ifndef INVENTORY_H
#define INVENTORY_H

#include <array>

/*
@brief Defines item types.
Can be used to index an inventory array.
*/
enum Items
{
    HEALTH_POTION,
    TORCHES,
    ARROWS,
    MAX_ITEMS
};

/*
@brief Returns the total number of items in the array.
@param const std::array<int, Items::MAX_ITEMS> &items: Array of items.
@return int: The total number of items.
*/
int countTotalItems(const std::array<int, Items::MAX_ITEMS> &items);

/*
@brief Returns the number of a specified item in an array of items.
@param const std::array<int, Items::MAX_ITEMS> &items: Array of items.
@param Items itemType: The type of item to count.
@return int: The number of the specified item in the given array.
*/
int countItem(const std::array<int, Items::MAX_ITEMS> &items, Items itemType);

#endif