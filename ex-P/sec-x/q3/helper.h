#ifndef HELPER_H
#define HELPER_H

/*
@brief Swaps to integers in place.
@param int &a: Reference to an integer to swap.
@param int &b: Reference to another integer to swap.
*/
void swap(int &a, int &b);

#endif