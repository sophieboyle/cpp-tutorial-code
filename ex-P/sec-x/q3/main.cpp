#include "helper.h"
#include <iostream>

int main()
{
    int a{2};
    int b{3};

    std::cout << "A is originally " << a << " and B is originally " << b << '\n';

    swap(a, b);

    std::cout << "A is now " << a << " and B is now " << b << '\n';

    return 0;
}