#ifndef STUDENTS_H
#define STUDENTS_H

#include <string>
#include <vector>

struct Student
{
    std::string name;
    int grade;
};

/*
@brief Asks the user to input a class size.
@return int: The integer class size requested from the user.
*/
int getClassSize();

/*
@brief Prompts the user to enter the values to build a Student struct.
@return Student: struct built by the values entered by the user.
*/
Student getStudent();

/*
@brief Constructs a vector of Students of a given classSize.
@param int classSize: The amount of student structs to put in the vector.
@return std::vector<Student>: The vector of students.
*/
std::vector<Student> getClassContents(int classSize);

/*
@brief Compares student structs by grade.
@param Student a: The first student to be compared.
@param Student b: The second student to be compared against.
@return bool: True if student a has a higher grade than student b, false otherwise.
*/
bool compareByGrade(const Student &a, const Student &b);


#endif