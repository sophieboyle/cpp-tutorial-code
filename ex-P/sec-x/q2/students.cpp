#include "students.h"
#include <string>
#include <iostream>
#include <vector>


int getClassSize()
{   
    int size{0};
    while (size <= 0)
    {
        std::cout << "Enter number of students: ";
        std::cin >> size;
    }
    return size; 
}


Student getStudent()
{
    std::string name{};
    int grade{};

    std::cout << "Enter a name: ";
    std::cin >> name;
    std::cout << "Enter a grade: ";
    std::cin >> grade;
    std::cin.ignore();

    return {name, grade};
}


std::vector<Student> getClassContents(int classSize)
{   
    std::vector<Student> students{};
    students.resize(classSize);
    for (int i{0}; i < classSize; i++)
    {
        students[i] = getStudent();
    }  
    return students;
}


bool compareByGrade(const Student &a, const Student &b)
{
    return a.grade > b.grade;
}