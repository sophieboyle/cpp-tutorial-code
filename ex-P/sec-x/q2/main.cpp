#include "students.h"
#include <vector>
#include <iostream>
#include <algorithm>

int main()
{
    int classSize{getClassSize()};
    std::vector<Student> students;
    students = getClassContents(classSize);
    std::sort(students.begin(), students.end(), compareByGrade);

    for (Student student : students)
        std::cout << "Student: " << student.name << " " << student.grade << '\n';

    return 0;
}