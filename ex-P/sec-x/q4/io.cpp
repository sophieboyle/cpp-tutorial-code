#include "io.h"
#include <string>
#include <iostream>

void outString(const std::string &str)
{
    auto begin{str.begin()};
    auto end{str.end()};

    for (auto i{begin}; i != end; i++)
        std::cout << *i << ' ';
    
    std::cout << '\n';
}