#ifndef IO_H
#define IO_H

#include <string>

/*
@brief Outputs string as whitespaced characters to console by iterating through the string with pointers.
@param std::string: String to be output.
*/
void outString(const std::string &str);

#endif