#include "calc.h"
#include <iostream>

int main()
{
    using namespace std;

    int a{};
    int b{};
    char op{};

    cout << "Enter an integer: ";
    cin >> a;
    cout << "Enter another integer: ";
    cin >> b;
    cout << "Enter an operator: ";
    cin >> op;

    cout << a << op << b << " = " << calculate(a, b, op);

    return 0;
}