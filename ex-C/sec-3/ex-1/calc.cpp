/*
@brief Performs different calculations on two operands depending on the operator
@int a: The first operand
@int b: The second operance
@char op: The operator
@retruns int: A calculation of a and b
*/
int calculate(int a, int b, char op)
{   
    // No breaks required as returns break fall through
    switch(op)
    {
        case '+':
            return a+b;
        case '-':
            return a-b;
        case '*':
            return a*b;
        case '/':
            return a/b;
    }
}