#include "animal.h"
#include <iostream>

using namespace std;


/*
@brief Takes an animal and returns a string representation of its type
@param Animal animal: The animal to be returned as a string
@returns string: The string representation of the animal
*/
string getAnimalName(Animal animal)
{
    switch(animal)
    {
        case (Animal::CAT):
            return "cat";
        case (Animal::CHICKEN):
            return "chicken";
        case (Animal::DOG):
            return "dog";
        case (Animal::GOAT):
            return "goat";
        case (Animal::OSTRICH):
            return "ostrich";
        case (Animal::PIG):
            return "pig";
        default:
            return "???";
    }
}


/*
@brief Prints the number of legs a given animal has to console
@param Animal animal: Animal to print to console
*/
void printNumberOfLegs(Animal animal)
{   
    switch(animal)
    {
        case (Animal::CAT):
        case (Animal::DOG):
        case (Animal::GOAT):
        case (Animal::PIG):
            cout << "A " << getAnimalName(animal) << " has 4 legs\n";
            break;
        case (Animal::CHICKEN):
        case (Animal::OSTRICH):
            cout << "A " << getAnimalName(animal) << " has 2 legs\n";
            break;
        default:
            cout << "???";
            break;
    }
}