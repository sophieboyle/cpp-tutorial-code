#include "animal.h"

int main()
{
    Animal myCat{Animal::CAT};
    Animal myChicken{Animal::CHICKEN};

    printNumberOfLegs(myCat);
    printNumberOfLegs(myChicken);

    return 0;
}