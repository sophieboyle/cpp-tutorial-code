#include <iostream>

void printChar()
{   
    int count{0};
    while (count<26)
    {   
        char myChar{97+count}; // To avoid the narrowing conversion, it's possible to simple increment the character
        std::cout << myChar << " " << static_cast<int>(myChar) << '\n';
        ++count;
    }
}