#ifndef PRINT_CHAR_H
#define PRINT_CHAR_H

/*
@brief Prints every character in the alphabet alongside their ASCII number to console.
*/
void printChar();

#endif