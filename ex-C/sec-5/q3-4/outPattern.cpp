#include <iostream>

void outPatternOne()
{
    int outer{0};
    while (outer < 5)
    {   
        int inner{5-outer};
        while (inner > 0)
        {   
            std::cout << inner << ' ';
            --inner;
        }
        std::cout << '\n';
        ++outer;
    }
}

void outPatternTwo()
{
    int outer{1};
    while (outer <= 5)
    {
        int inner{5};
        while (inner > 0)
        {   
            if (outer/inner >= 1)
                std::cout << inner;
            else
                std::cout << ' ';
            --inner;
        }
        std::cout << '\n';
        ++outer;
    }
}