#ifndef OUT_PATTERN_H
#define OUT_PATTERN_H

/*
@brief Outputs a triangular pattern from the top left hand corner
*/
void outPatternOne();

/*
@brief Outputs a triangular pattern from the bottom right hand corner
*/
void outPatternTwo();

#endif