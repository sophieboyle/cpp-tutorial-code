#include <iostream>

/*
Write a for loop that prints every even number from 0 to 20.
*/
int main()
{   
    // To avoid checking if the number is even
    // We can instead increment i by 2
    for (int i=0; i<=20; i+=2)
        std::cout << i << '\n';
}