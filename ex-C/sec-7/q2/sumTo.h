#ifndef SUM_TO_H
#define SUM_TO_H

/*
@brief Sums up all values to a given value
@param int value: The value to be summed to
@return int: The sum of values
*/
int sumTo(int value);

#endif