#include "valcheck.h"
#include <iostream>
#include <limits>

void guessNumber(int num, int guessAmount)
{   
    std::cout << "Let's play a game. I'm thinking of a number. ";
    std::cout << "You have " << guessAmount << " tries to guess.\n";

    for (int guesses{1}; guesses<guessAmount+1; ++guesses)
    {   
        int userGuess{getUserGuess(guesses)};
        
        if(userGuess < num)
            std::cout << "Your guess is too low.\n";
        else if (userGuess > num)
            std::cout << "Your guess is too high.\n";
        else
        {
            std::cout << "Correct! You win!\n";
            return;
        }
    }

    std::cout << "Sorry, you lose. The correct number was " << num << '\n';
    return;
}


bool playAgain()
{
    while (true)
    {   
        char userChoice;
        std::cout << "Would you like to play again (y/n)? ";
        std::cin >> userChoice;
        
        if (userChoice=='y')
            return true;
        else if (userChoice=='n')
            return false;
        else
            continue;
    }
}


int getUserGuess(int guessNum)
{
    while (true)
    {   
        std::cout << "Guess #" << guessNum << ": ";
        int userGuess{};
        std::cin >> userGuess;

        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        else
        {   
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            return userGuess;
        }
    }
}