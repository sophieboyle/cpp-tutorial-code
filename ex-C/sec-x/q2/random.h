#ifndef RANDOM_H
#define RANDOM_H

#include <random>
#include <ctime>

/*
@brief Returns a random number between the given minimum and maximum
@param int min: The minimum value a random number can be
@param int max: The maximum value a random number can be
@return int: The random integer
*/
int getRandomNumber(int min, int max);

#endif