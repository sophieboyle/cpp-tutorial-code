#ifndef VALCHECK_H
#define VALCHECK_H

/*
@brief Prompts user to guess a number for a given amount of guesses.
Outputs whether they are correct/incorrect.
@param int num: The number to be guessed.
@param int guessAmount: The number of guesses allowed.
*/
void guessNumber(int num, int guessAmount);

/*
@brief Prompts the user as to whether they wish to continue or not, until they enter y/n.
@returns bool: True if they enter 'y', false if they enter 'n'.
*/
bool playAgain();

/*
@brief Get user's guess, checks against invalid input.
@param int guessNum: The guess that the user is on.
@returns int: User's guess.
*/
int getUserGuess(int guessNum);

#endif