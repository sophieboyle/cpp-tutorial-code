#include "random.h"
#include "valcheck.h"
#include <iostream>

int main()
{
    while (true)
    {
        int randNum{getRandomNumber(1,100)};
        guessNumber(randNum, 7);

        bool cont{playAgain()};

        if (cont==true)
            continue;
        else
            return 0;
    }
}