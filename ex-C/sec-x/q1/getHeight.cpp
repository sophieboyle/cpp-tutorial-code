#include "sci.h"
#include <cmath>

/*
@brief returns The current height after falling for some seconds, note this may produce a negative
@param height The initial height of the tower
@param seconds How many seconds have passed since dropped from tower top
*/
double getHeight(double height, int seconds)
{
    // Returns current height
    return height - (sci::gravity * (seconds * seconds) / 2);
}