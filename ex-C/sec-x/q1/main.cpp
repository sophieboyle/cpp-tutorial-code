#include "getHeight.h"
#include "io.h"

int main()
{   
    double height{getTowerHeight()};
    // user enters height of tower

    // output height of ball after 0,1,2,3,4,5 secs
    // only if the remaining height is not <=0
    /*outResult(getHeight(height, 0), 0);
    outResult(getHeight(height, 1), 1);
    outResult(getHeight(height, 2), 2);
    outResult(getHeight(height, 3), 3);
    outResult(getHeight(height, 4), 4);
    outResult(getHeight(height, 5), 5);*/

    double currentHeight{height};
    int second{0};
    while (currentHeight>0)
    {   
        currentHeight = getHeight(height, second);
        outResult(currentHeight, second);
        ++second;
    }

    return 0;
}