/*
@brief Returns true if the integer is even, and false if odd
@param userInt, the integer to be checked
@return bool
*/
bool isEven(int userInt)
{
    if ((userInt%2)==0)
        return true;
    else
        return false;
}