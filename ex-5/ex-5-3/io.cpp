#include  <iostream>

/*
@brief Prompts user to enter an integer
@return Integer provided by user
*/
int getInput()
{   
    int x{};
    std::cout << "Enter an integer: ";
    std::cin >> x;
    return x;
}

/*
@brief Prints to console whether the integer is even or odd
@param Boolean which is true if even, or false if odd
@param Integer which was chosen by the user
*/
void writeAns(bool isEven, int userInt)
{
    if (isEven)
        std::cout << userInt << " is Even.\n";
    else
        std::cout << userInt << " is Odd.\n";
}