#include "io.h"
#include "checkEven.h"

/*
Gets integer from user
Checks if the integer is even
Outputs result
*/
int main()
{
    int userInt{getInput()};
    writeAns(isEven(userInt), userInt);
    return 0;
}