#include "io.h"
#include <iostream>

// Returns an integer from console input
int readNumber(){
    int x{};
    std::cout << "Enter an integer: ";
    std::cin >> x;
    return x;
}

// Writes an integer on console
void writeAnswer(int x){
    std::cout << x << '\n';
}