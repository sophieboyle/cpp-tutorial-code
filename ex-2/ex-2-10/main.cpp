#include <iostream>

#define EIGHT // This should be ignored in bleh.cpp, as preprocessor 
            // only reads a file at a time
int bleh(); // This declaration is required for the compiler

int main()
{   
    // Should initialise x with 9
    int x{bleh()};
    std::cout << "Your number is: " << x;
    return 0;
}