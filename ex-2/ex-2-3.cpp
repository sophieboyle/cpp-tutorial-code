#include <iostream>

int doubleNumber(int x)
{
    return x * 2;
}

int main()
{
    int x{};
    std::cout << "Enter an integer: ";
    std::cin >> x;
    std::cout << doubleNumber(2);
    return 0;
}