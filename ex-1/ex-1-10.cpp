#include <iostream>

int main()
{   
    int x{};
    std::cout << "Enter a number please: ";
    std::cin >> x;
    std::cout << "Double " << x << " is " << x * 2 << '\n';
    std::cout << "Triple " << x << " is " << x * 3;
    return 0;
}