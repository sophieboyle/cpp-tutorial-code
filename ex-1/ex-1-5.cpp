#include <iostream>

int main()
{
    std::cout << "Enter a number";
    int x{};
    std::cin >> x;
    std::cout << "You entered: " << x << '\n';
    return 0;
}

/*
Entering a char results in x=0
Entering a float results in x=0
It takes small negatives
Entering a string results in x=0
Entering 3000000000 results in x=2147483647
x has been truncated to the largest value an int can store
*/