#include <iostream>

int main()
{
    int x{};
    int y{};
    
    // Assign first user input
    std::cout << "Enter an integer: ";
    std::cin >> x;

    // Assign second user input
    std::cout << "Enter another integer: ";
    std::cin >> y;

    // Perform and output operations
    std::cout << x << " + " << y << " is " << x + y << '\n';
    std::cout << x << " - " << y << " is " << x-y;

    return 0;
}