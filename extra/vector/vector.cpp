/* TODO: reserve(), resize(), push_back()
and if you wanna get bonus points
implement the operator overloads + emplace_back*/

template <typename T>
class Vector {

    T *vector{};
    size_t length{};

    /**
    * @brief Constructs vector of a given size.
    * @param int: Initial length of the vector.
    */
    Vector(size_t count = 0){
        vector = new T[count];
    }

    /**
    * @brief Returns the length of the vector.
    * @return int: Length.
    */
    int size(){
        return length;
    }

    /**
    * @brief Returns the capacity of the vector.
    * @return int: Capacity.
    */
    int capacity(){
        return length * sizeof(T);
    }

    /**
    * @brief Returns whether the vector is empty.
    * @return bool: True if empty, false otherwise.
    */
    bool empty(){
        return length == 0;
    }
};