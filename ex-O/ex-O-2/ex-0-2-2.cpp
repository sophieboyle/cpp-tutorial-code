#include <iostream>
#include <bitset>

std::bitset<4> rotl(std::bitset<4> bits)
{
    int droppedBit{bits.test(3)};
    bits <<= 1;
    
    if (droppedBit)
        bits.set(0);
    // else statement actually isn't necessary,
    // the leftmost bit will always be 0 after being shifted
    else
        bits.reset(0);
    
    return bits;
}

int main()
{
    std::bitset<4> bits1{0b0001};
    std::cout << rotl(bits1) << '\n';

    std::bitset<4> bits2{0b1001};
    std::cout << rotl(bits2) << '\n';

    return 0;
}