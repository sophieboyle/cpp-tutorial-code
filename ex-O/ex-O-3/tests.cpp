#include <cstdint>

/*
@brief Sets input article as viewed
@param article, article to be set
@param mask, bitmask for viewing
@returns int, newly set article
*/
int setViewed(int article, int mask)
{
    return article |= mask;
}

/*
@brief Checks if the article was deleted
@param article, article to check
@param mask, bitmask for deletion
@returns bool, true if deleted and false otherwise
*/
bool checkDeleted(int article, int mask)
{
    return (article & mask) ? true : false;
}

/*
@brief Clears article as a favorite
@param article, article to be cleared from being favorite
@param mask, bitmask for being a favorite
@returns int, new article with favorite removed
*/
int removeFavorite(int article, int mask)
{
    return article &= ~mask;
}