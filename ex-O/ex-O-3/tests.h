#ifndef TESTS_H
#define TESTS_H

int setViewed(int article, int mask);
bool checkDeleted(int article, int mask);
int removeFavorite(int article, int mask);

#endif