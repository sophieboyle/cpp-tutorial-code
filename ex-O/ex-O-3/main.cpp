#include "tests.h"
#include <cstdint>
#include <iostream>

int main()
{
    constexpr std::uint_fast8_t option_viewed{0x01};
    constexpr std::uint_fast8_t option_edited{0x02};
    constexpr std::uint_fast8_t option_favorited{0x04};
    constexpr std::uint_fast8_t option_shared{0x08};
    constexpr std::uint_fast8_t option_deleted{0x80};

    std::uint_fast8_t myArticleFlags{};

    std::cout << "My article is: " << static_cast<int>(myArticleFlags) << '\n';
    // Setting the article as viewed
    myArticleFlags = setViewed(myArticleFlags, option_viewed);
    std::cout << "My article is now: " << static_cast<int>(myArticleFlags) << '\n'; 
    // Checking if the article is deleted
    std::cout << "My article has: " << ((checkDeleted(myArticleFlags, option_deleted)) ? "been deleted\n" : "not been deleted\n");
    // Removing favourite
    myArticleFlags = removeFavorite(myArticleFlags, option_favorited);
    std::cout << "My article is now: " << static_cast<int>(myArticleFlags) << '\n';

    return 0;
}