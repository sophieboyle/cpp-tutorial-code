#include <cstdint>
#include <iostream>

/*
@brief Checks if a number can be subtracted by another number
@int userDecimal, the number which may contain another number
@int checkingNo, the number may be contained
*/
bool checkIfIn(int userDecimal, int checkingNo)
{
    if ((userDecimal - checkingNo) >=0)
        return true;
    else
        return false;
}

/*
@brief takes a decimal, prints 1 to console, and decrements the decimal
@param int decimal, the user number that is to be decremented
@param int decrement, the number to subtract by
@return int, the decimal number subtracted by the decrement
*/
int writeAndDecrement(int decimal, int decrement)
{
    std::cout << '1';
    return decimal-decrement;
}

/*
@brief takes a decimal and decrement value, and depending on the boolean outputs different values to console and chooses whether to decrement
@param bool bit, true to print 1 and decrement, false to print 0
@param int decimal, to possibly decrement
@param int decrement, integer to possibly subtract by
*/
int writeAnswer(bool bit, int decimal, int decrement)
{
    if (bit)
        return writeAndDecrement(decimal, decrement);
    else
    {
        std::cout << '0';
        return decimal;
    }
}