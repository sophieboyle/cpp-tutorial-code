#ifndef HELPER_H
#define HELPER_H

bool checkIfIn(int userDecimal, int checkingNo);
int writeAndDecrement(int decimal, int decrement);
int writeAnswer(bool bit, int decimal, int decrement);

#endif