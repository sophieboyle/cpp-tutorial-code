#include "helper.h"
#include <iostream>
#include <cstdint>

/*
Takes user input of a decimal number between 0 and 225
Converts it to an unsigned binary number
*/
int main()
{
    // Get decimal number from user
    int userDecimal{};
    std::cout << "Enter a decimal number: ";
    std::cin >> userDecimal;

    userDecimal = writeAnswer(checkIfIn(userDecimal, 128), userDecimal, 128);
    userDecimal = writeAnswer(checkIfIn(userDecimal, 64), userDecimal, 64);
    userDecimal = writeAnswer(checkIfIn(userDecimal, 32), userDecimal, 32);
    userDecimal = writeAnswer(checkIfIn(userDecimal, 16), userDecimal, 16);
    userDecimal = writeAnswer(checkIfIn(userDecimal, 8), userDecimal, 8);
    userDecimal = writeAnswer(checkIfIn(userDecimal, 4), userDecimal, 4);
    userDecimal = writeAnswer(checkIfIn(userDecimal, 2), userDecimal, 2);
    userDecimal = writeAnswer(checkIfIn(userDecimal, 1), userDecimal, 1);

    return 0;
}