#include "isPrime.h"
#include <iostream>

int main()
{
    // get int from user
    int x{};
    std::cout << "Enter a single digit integer: ";
    std::cin >> x;

    // check if int is prime
    // tell user if it is prime
    if (isPrime(x))
        std::cout << x << " is prime";
    else
        std::cout << x << " is not prime";

    return 0;
}