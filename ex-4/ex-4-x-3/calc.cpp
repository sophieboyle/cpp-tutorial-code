#include "calc.h"
#include <iostream>

void calc(double x, double y, char z)
{
    std::cout << x << z << y << " is ";
    if (z=='+')
        std::cout << x+y;
    else if (z=='-')
        std::cout << x-y;
    else if (z=='*')
        std::cout << x*y;
    else if (z=='/')
        std::cout << x/y;
    else
        std::cout << "\nInvalid operator";
}