#include "io.h"
#include <iostream>

double getDouble()
{
    double x{};
    std::cout << "Enter a double: ";
    std::cin >> x;
    return x;
}

char getOperation()
{
    char x{};
    std::cout << "Enter an operation: ";
    std::cin >> x;
    return x;
}