#include "io.h"
#include "calc.h"

int main()
{
    // user enters two doubles
    double x{getDouble()};
    double y{getDouble()};

    // user enters operation
    char op{getOperation()};

    // prints calculation of two doubles and operation
    calc(x, y, op);

    return 0;
}