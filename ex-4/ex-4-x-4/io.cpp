#include <iostream>
#include <iomanip>

double getTowerHeight()
{   
    double height{};
    std::cout << "Enter a height: ";
    std::cin >> height;
    return height;
}

void outResult(double height, int seconds)
{
    std::cout << std::setprecision(4);
    if (height > 0)
        std::cout << "At " << seconds << " seconds, the ball is at height: " << height << " meters\n";
    else
        std::cout << "At " << seconds << " seconds, the ball is on the ground.\n";
}