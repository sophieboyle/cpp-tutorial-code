#include "calculator.h"
#include <iostream>
#include <functional>


int getInteger()
{   
    int x{};
    std::cout << "Enter an integer: ";
    std::cin >> x;
    std::cin.ignore();
    return x; 
}


char getOperator()
{
    char op{};
    std::cout << "Enter an operator: ";
    std::cin >> op;
    std::cin.ignore();
    return op;
}


inline int add(int x, int y)
{
    return x+y;
}


inline int subtract(int x, int y)
{
    return x-y;
}


inline int multiply(int x, int y)
{
    return x*y;
}


inline int divide(int x, int y)
{
    return x/y;
}


arithmeticFcn getArithmeticFunction(char op)
{
    switch (op)
    {
        case ('+'):
            return add;
        case ('-'):
            return subtract;
        case ('*'):
            return multiply;
        case ('/'):
            return divide;
        default:
            return add;
    }
}