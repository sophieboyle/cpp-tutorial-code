#include "calculator.h"
#include <functional>
#include <iostream>

int main()
{
    int x{getInteger()};
    char op{getOperator()};
    int y{getInteger()};

    arithmeticFcn method{getArithmeticFunction(op)};

    int result{method(x, y)};
    std::cout << result;

    return 0;
}