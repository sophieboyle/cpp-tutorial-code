#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <functional>

using arithmeticFcn = std::function<int(int,int)>;

/*
@brief Requests an integer from the user.
@param int: retrieved from user.
*/
int getInteger();

/*
@brief Requests an operator from the user.
@param char: retrieved from user.
*/
char getOperator();

/*
@brief Adds two given integers.
@param int: First operand.
@param int: Second operand.
*/
inline int add(int x, int y);

/*
@brief Subtracts two given integers.
@param int: First operand.
@param int: Second operand.
*/
inline int subtract(int x, int y);

/*
@brief Multiplies two given integers.
@param int: First operand.
@param int: Second operand.
*/
inline int multiply(int x, int y);

/*
@brief Divides two given integers.
@param int: Numerator.
@param int: Denominator.
*/
inline int divide(int x, int y);

/*
@brief Returns a function pointer based on the given operator.
@param char: The operator.
@return arithmeticFcn: A pointer to an arithmetic function.
If the operator does not match a defined arithmetic function, returns add.
*/
arithmeticFcn getArithmeticFunction(char op);

#endif