#include "factorial.h"
#include <vector>


int factorial(int num)
{
    if (num == 0)
        return 1;
    else
        return (num * factorial(num-1));
}


int memoizedFactorial(int num)
{   
    static std::vector<int> factorials{1};

    if (num == 0)
        return 1;
    else if (num < factorials.size())
        return factorials[num];
    else
    {   
        factorials.push_back(num * factorial(num-1));
        return (num * factorial(num-1));
    }
}