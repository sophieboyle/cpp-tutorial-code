#ifndef FACTORIAL_H
#define FACTIORIAL_H

/*
@brief Recursively calculate the factorial of a given number.
@param int num: The number to calculate the factorial of.
@param int: The factorial of the num.
*/
int factorial(int num);

/*
@brief Recursively calculate the factorial of a given number (optimised).
@param int num: The number to calculate the factorial of.
@return int: The factorial of the num.
*/
int memoizedFactorial(int num);

#endif