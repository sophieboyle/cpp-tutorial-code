#include "factorial.h"
#include <iostream>

int main()
{
    for (int i{0}; i <= 7; ++i)
        std::cout << i << "! = " << memoizedFactorial(i) << '\n';
}