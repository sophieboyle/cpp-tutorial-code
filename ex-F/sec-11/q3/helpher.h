#ifndef HELPER_H
#define HELPER_H

/*
@brief Gets an integer from user.
@return int: Selected by user.
*/
int getInteger();

#endif