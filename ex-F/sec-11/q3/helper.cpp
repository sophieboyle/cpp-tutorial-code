#include "helper.h"
#include <iostream>


int getInteger()
{
    int x{};
    std::cout << "Enter an integer: ";
    std::cin >> x;
    return x;
}


void outBinary(int x)
{
    std::cout << (x%2);

    // Termination case:
    if (x==1)
        return;

    outBinary(x/2);
}