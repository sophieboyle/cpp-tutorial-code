#ifndef SUM_H
#define SUM_H

#include <vector>

/*
@brief Retrieves integers from one user input line.
@return std::vector<int>: Vector of integers from console.
*/
std::vector<int> getIntegers();

/*
@brief Sums integers from console until newline reached.
@param int num: Integer to add to.
@return int: Sum of integers.
*/
int sum(std::vector<int> digits);

#endif