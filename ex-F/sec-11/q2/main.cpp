#include "sum.h"
#include <iostream>


int main()
{
    std::vector<int> userInts{getIntegers()};
    int x{sum(userInts)};
    std::cout << "Your number summed is: " << x << '\n';

    return 0;
}