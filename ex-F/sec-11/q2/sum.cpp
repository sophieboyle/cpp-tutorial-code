#include "sum.h"
#include <iostream>
#include <vector>
#include <string>


std::vector<int> getIntegers()
{
    std::vector<int> userInts{};
    std::string userStr{};

    std::cout << "Enter integers: ";
    std::getline(std::cin, userStr);

    for (auto digit : userStr)
        userInts.push_back(digit - '0');
    
    return userInts;
}


int sum(std::vector<int> digits)
{
    if (digits.size() == 0)
        return 0;
    else
    {   
        int curDigit{digits[digits.size()-1]};
        digits.pop_back();
        return curDigit + sum(digits);
    }
    
}


/*
int sum(int num)
{   
    char nextDigit{};
    std::cin >> nextDigit;
    std::cout << nextDigit;

    if (std::cin.fail())
        return num;
    else
    {
        int nextInt{nextDigit - '0'};
        return num+nextInt;
    }
}
*/